# Media Library Translate

Media Library Translate intends to improve the default translation user 
experience for media into a more intractive modal, providing a fast 
and enhanced user experience.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/media_library_translate).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/search/media_library_translate).

## Table of contents

- Requirements
- Installation
- How to use
- Maintainers

## Requirements

This module requires the following modules:

- [Media Library](https://www.drupal.org/project/media_library)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## How To Use

## 1. Enable media translation
Access the entity translation form at (/admin//admin/config/regional/content-language) 
after enabling Content translation.

## 2. Add more then one Language on your multilingual site. 
Access the form at (/admin/config/regional/language).

## 3. Enable the translate button on the Media Library widget. 
Once the module is enabled, click on the Manage Form settings gear 
for the relevant Media reference field using the Media Library widget
and enable the "Show translation button" option.
Access the form at 
(/admin/structure/types/manage/{{ Content-Type }}/form-display).

## Maintainers

Current maintainers:

- [Ashish Dwivedi (adwivedi008)](https://www.drupal.org/u/adwivedi008)
- [Keshav patel (keshav-patel)](https://www.drupal.org/u/keshav-patel)
- [Pradhuman Jain (pradhumanjainOSL)](https://www.drupal.org/u/pradhumanjainOSL)
